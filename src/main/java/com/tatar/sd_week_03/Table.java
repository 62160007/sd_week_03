/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_03;

/**
 *
 * @author Dell 62160007 Thanawat Thongtitcharoen Sec 2
 */
public class Table {

    private char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print((i + 1) + " ");
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            return true;
        }
        return false;
    }

    public boolean checkWin() {
        int result = 0;
        result += checkCol(table);
        result += checkRow(table);
        result += checkX1(table);
        result += checkX2(table);
        if (result > 0) {
            return false;
        }
        return true;
    }

    public static int checkRow(char table[][]) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == 'X' && table[i][1] == 'X'
                    && table[i][2] == 'X') {
                return 1;
            } else if (table[i][0] == 'O' && table[i][1] == 'O'
                    && table[i][2] == 'O') {
                return 1;
            }
        }
        return 0;
    }

    public static int checkCol(char table[][]) {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == 'X' && table[1][i] == 'X'
                    && table[2][i] == 'X') {
                return 1;
            } else if (table[0][i] == 'O' && table[1][i] == 'O'
                    && table[2][i] == 'O') {
                return 1;
            }
        }
        return 0;
    }

    public static int checkX1(char table[][]) {
        if (table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X') {
            return 1;
        } else if (table[0][0] == 'O' && table[1][1] == 'O'
                && table[2][2] == 'O') {
            return 1;
        }
        return 0;
    }

    public static int checkX2(char table[][]) {
        if (table[2][0] == 'X' && table[1][1] == 'X' && table[0][2] == 'X') {
            return 1;
        } else if (table[2][0] == 'O' && table[1][1] == 'O'
                && table[0][2] == 'O') {
            return 1;
        }
        return 0;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    public void setStatWinLose(int counter) {
        if (counter == 9) {
            playerO.draw();
            playerX.draw();
        } else if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    public void reTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                table[i][j] = '-';
            }
        }
        currentPlayer = playerX;
    }
}
